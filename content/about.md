---
title: "About"
date: 2018-02-11T14:23:07-05:00
draft: false
---

# Welcome to Data Science.com
## About Us

A dentist performs many important duties in the maintenance of oral health. Check-ups are a major part of the job and are important for patients keeping on top of their oral care. 

In addition to general check-ups, general dentistry involves basic treatments that can be done at a regular dental office.

> Such procedures might include filling a tooth, performing a minor surgical procedure such as a tooth extraction, and cleaning and polishing teeth.

A dentist will encourage patients to practice good dental care by brushing and flossing and getting regular cleanings as these good habits can help prevent cavities and other tooth and gum diseases

