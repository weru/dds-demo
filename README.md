### Where to find the *pages* and *Posts* 

All post files (articles) will be found inside the **content/post** directory, while all pages will be found on the top level of **content** directory

### What if I want to edit the menu?

To edit the menu, look inside the **config.toml** file. The top most navigation's menu links are found inside this file. They are specified like so

```yaml
[[menu.main]]
    name = "DDS Home"
    weight = -120
    url = "/"
[[menu.main]]
    name = "About"
    weight = -110
    identifier = "about"
    url = "/about/"
[[menu.main]]
    name = "Links"
    weight = -100
    url = "/links/
```

### Editing the individual posts and pages

Virtually all content you will add to your site will be written in markdown. As such, you will need to know how to write your content using [markdown syntax](http://assemble.io/docs/Cheatsheet-Markdown.html).

In my experience, markdown format is incredibly simple in nature, and hence easy to pick up. After 15 minutes of going through [this guide](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), you should be able to write in markdown like a pro.

That said, there are different markdown flavours, but the differences are minor. You will find that you will fulfill most of your use cases with basics. 

### Where will I store my media assets?

Store all your images in the **static** folder. You may create additional subfolders as you go by; feel free to name them as you wish. 

To reference these files in your posts, link to the path where you stored the file, ommitting the word *static*.

For example, if I have a PNG image (example.png) inside a subfolder **icons**, I would reference the image in my post/pages as

```markdown
![](/icons/example.png)
```

Notice, I did not prepend */static/* on the file path.

### How do I add categories and tags to my posts?

List these in your posts front matter. Like so

```yaml
+++
categories
  - "Category1"
  - "Category2"
tags
  - "Tag1"
+++
```
Notice, you're at liberty to specify one or more category or tags. To is totally left to your discretion.

### Deployment options?

I take that you're good with git workflow. I would recommend using either

1. [Netlify cms](https://www.netlify.com/)
2. [Forestry.io](https://forestry.io)

Of course there are [several others](https://gohugo.io/hosting-and-deployment/), but from my experience, I find that these two automate the process for you ... thus enabling you to concentrate on what is important (blogging).

### Have further queries?

Feel free to engage me. Happy blogging with Hugo!

Thanks.